package com.std.model;

import java.util.Observable;
import java.util.Observer;

/**
 * SetObserver is an Observer that observes apptTmplSet and apptSet
 * for any updates, marks the model as changed, and forwards those 
 * updates to the model's listeners.
 * 
 * @author xxx
 *
 */
class SetObserver implements Observer {
    private CalendarModel that;
    
    public SetObserver(CalendarModel that){
        this.that = that;
    }


    /**
     * This method is called whenever the observed object is changed. An
     * application calls an <tt>Observable</tt> object's
     * <code>notifyObservers</code> method to have all the object's
     * observers notified of the change.
     *
     * @param   o     the observable object.
     * @param   arg   an argument passed to the <code>notifyObservers</code>
     *                 method.
     * @see <a href="http://java.sun.com/javase/6/docs/api/java/util/Observer.html#update(java.util.Observable,%20java.lang.Object)">Observer.update(Observable, Object)</a>
     */
    public void update(Observable o, Object arg) {
        that.setDiffFile(true);
        
        that.notifyObservers();

    }
}
