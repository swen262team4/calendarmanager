package com.std.model;

import java.util.Observable;
import java.util.Observer;

/**
 * ElementObserver is an Observer that watches an object
 * and forwards their updates to the CalendarModel
 * observers.
 * 
 * @author xxx
 *
 */
class ElementObserver implements Observer {

    private boolean diffFile;
    private final CalendarModel that;
    public ElementObserver(boolean diffFile, CalendarModel that){
        this.diffFile = diffFile;
        this.that = that;
    }
    /**
     * This method is called whenever the observed object is changed. An
     * application calls an <tt>Observable</tt> object's
     * <code>notifyObservers</code> method to have all the object's
     * observers notified of the change.
     *
     * @param   o     the observable object.
     * @param   arg   an argument passed to the <code>notifyObservers</code>
     *                 method.
     * @see <a href="http://java.sun.com/javase/6/docs/api/java/util/Observer.html#update(java.util.Observable,%20java.lang.Object)">Observer.update(Observable, Object)</a>
     */
    public void update(Observable o, Object arg) {
        diffFile = true;
        //that.setChanged();
        that.notifyObservers(o);
    }
}
