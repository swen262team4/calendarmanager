package com.std.model;

import java.util.Observable;
import java.util.Observer;

import com.std.model.appointment.AppointmentTemplate;
import com.std.util.ObservableSet;

/**
 * ApptTmplSetObserver is an Observer that observes apptTmplSet
 * for any removal updates, and removes any reference appointments
 * to that template from apptSet.
 * 
 * @author xxx
 *
 */
class ApptTmplSetObserver implements Observer {
    private CalendarModel that;
    public ApptTmplSetObserver(CalendarModel that){
        this.that = that;
    }

    /**
     * This method is called whenever the observed object is changed. An
     * application calls an <tt>Observable</tt> object's
     * <code>notifyObservers</code> method to have all the object's
     * observers notified of the change.
     *
     * @param   o     the observable object.
     * @param   arg   an argument passed to the <code>notifyObservers</code>
     *                 method.
     * @see <a href="http://java.sun.com/javase/6/docs/api/java/util/Observer.html#update(java.util.Observable,%20java.lang.Object)">Observer.update(Observable, Object)</a>
     */
    public void update(Observable o, Object arg) {
        ObservableSet<?> set = (ObservableSet<?>)o;
        AppointmentTemplate apptTmpl = (AppointmentTemplate)arg;
        if(!set.contains(apptTmpl)) 
            that.removeReferences(apptTmpl);
    }
}